#!/bin/bash
# Filters out invalid users from the user list.

IN_FILE="to_total"
USER_FILTERED_FILE="to_total_filtered"
USER_FILTERED_OUT_FILE="to_total_filtered_out"
rm -rf $USER_FILTERED_FILE
rm -rf $USER_FILTERED_OUT_FILE
while read p; do
    #[[ $p =~ ^[a-z][-a-z0-9_\.]*\$?$ ]] || echo "no $p"

    # Except nobody.
    if [[ $p == "nobody" ]]; then
	echo "$p" >> $USER_FILTERED_OUT_FILE
	continue
    fi

    # Ubuntu
    #[[ $p =~ ^[a-z0-9_.][-a-z0-9_\.]{0,31}\$?$ ]] && echo "$p" >> $USER_FILTERED_FILE
    #[[ $p =~ ^[a-z0-9_.][-a-z0-9_\.]{0,31}\$?$ ]] || echo "$p" >> $USER_FILTERED_OUT_FILE

    # Fedora
    [[ $p =~ ^[a-z_.][-a-z0-9_\.]{0,31}\$?$ ]] && echo "$p" >> $USER_FILTERED_FILE
    [[ $p =~ ^[a-z_.][-a-z0-9_\.]{0,31}\$?$ ]] || echo "$p" >> $USER_FILTERED_OUT_FILE
done < $IN_FILE

# Exclude kitchen-l becuase it is not included in the dataset because of parsing error.
cat ./kitchen-l_users >> $USER_FILTERED_OUT_FILE
